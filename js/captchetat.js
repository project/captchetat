(($, Drupal, once) => {
  Drupal.behaviors.captchetat = {
    attach(context) {
      once('captchetat', '#captchetat', context).forEach(() => {
        // Nothing to do.
      });
    },
  };
})(jQuery, Drupal, once);
