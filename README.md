# CaptchEtat

The CaptchEtat module integrates a CAPTCHA webform element by connecting to the CaptchEtat API. 
This CAPTCHA webform element enhances security measures by verifying that the user is human,
thereby preventing automated bots from submitting forms or accessing certain functionalities.

For a full description of the module, visit the [project page](https://www.drupal.org/project/captchetat).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/captchetat).

For more information about the CaptchEtat API, visit [API CaptchEtat](https://api.gouv.fr/les-api/api-captchetat).

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Development](#development)
- [Maintainers](#maintainers)
- [Supporting organizations](#supporting-organizations)

## Requirements

This module requires the following modules:

- [Webform](https://www.drupal.org/project/webform)

## Installation

1. Download and install the module as you would normally do:
   - Using the Drupal admin interface, or
   - Using Drush: `drush en captchetat`
2. Enable the module:
   - Navigate to **Administration** > **Extend** and enable the module.

## Configuration

1. Navigate to **Administration** > **Configuration** > **CAPTCHETAT** to access the configuration page.
2. On the configuration page, enter the required API keys and URLs from [API CaptchEtat](https://api.gouv.fr/les-api/api-captchetat):
   - API URL
   - OAuth URL
   - Client ID
   - Client Secret
3. Save the configuration.

## Development

You can add the CaptchEtat element to your webforms like other fields.

To disable CaptchEtat validation in your local or test environment, add the following line to `settings.php`:
```php
$settings['disable_captchetat'] = TRUE;
```

## Maintainers

- Elia Wehbe - [ewehbe](https://www.drupal.org/u/ewehbe)
- Lara Zaki - [lara_z](https://www.drupal.org/u/lara_z)
- Georges ADWAN - [georges-adwan](https://www.drupal.org/u/georges-adwan)

## Supporting organizations:

- bluedrop.fr - ebizproduction - [bluedrop.fr - ebizproduction](https://www.drupal.org/bluedropfr-ebizproduction)
