<?php

namespace Drupal\captchetat\Controller;

use Drupal\captchetat\Service\CaptchetatServiceInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * This controller provides different connections to API CaptchEtat.
 */
class CaptchetatController extends ControllerBase {

  /**
   * The CaptchEtat API service.
   *
   * @var \Drupal\captchetat\Service\CaptchetatServiceInterface
   */
  protected CaptchetatServiceInterface $captchetat;

  /**
   * Constructs a new CaptchetatController object.
   *
   * @param \Drupal\captchetat\Service\CaptchetatServiceInterface $captchetat
   *   The CaptchEtat API service.
   */
  public function __construct(CaptchetatServiceInterface $captchetat) {
    $this->captchetat = $captchetat;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): CaptchetatController {
    return new static(
      $container->get('captchetat.captchetat'),
    );
  }

  /**
   * Proxy to Captchetat API simple-captcha-endpoint.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   HTTP Response for the CaptchEtat element.
   */
  public function getCaptcha(Request $request): Response {
    $token = $this->captchetat->getApiToken();
    return $this->captchetat->getCaptcha($token, $request);
  }

}
