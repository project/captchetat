<?php

namespace Drupal\captchetat\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'captchetat' element.
 *
 * @WebformElement(
 *   id = "captchetat",
 *   label = @Translation("CaptchEtat"),
 *   description = @Translation("Provides a CaptchEtat form element that determines whether the user is human."),
 *   category = @Translation("Advanced elements"),
 * )
 *
 * @see \Drupal\captchetat\Element\WebformCaptchetatElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformCaptchetatElement extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'flex' => 1,
      // Default CAPTCHA style.
      'captcha_style' => 'captcha',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    // Retrieve the selected CAPTCHA style from the element's properties.
    $captcha_style = $element['#captcha_style'] ?? 'captcha';

    // Configure the CAPTCHA element based on the selected style.
    switch ($captcha_style) {
      case 'numerique6_7Captcha':
        $element['#captcha_question'] = $this->t('Enter 6 to 7 numeric characters');
        break;

      case 'alphabetique6_7Captcha':
        $element['#captcha_question'] = $this->t('Enter 6 to 7 alphabetical characters');
        break;

      case 'alphanumerique12Captcha':
        $element['#captcha_question'] = $this->t('Enter 12 alphanumeric characters');
        break;

      case 'alphabetique12Captcha':
        $element['#captcha_question'] = $this->t('Enter 12 alphabetical characters');
        break;

      case 'numerique12Captcha':
        $element['#captcha_question'] = $this->t('Enter 12 numeric characters');
        break;

      case 'alphanumerique4to6LightCaptcha':
        $element['#captcha_question'] = $this->t('Enter 4 to 6 alphanumeric characters (light)');
        break;

      case 'alphanumerique6to9LightCaptcha':
        $element['#captcha_question'] = $this->t('Enter 6 to 9 alphanumeric characters (light)');
        break;

      case 'captcha':
      default:
        $element['#captcha_question'] = $this->t('Enter 6 to 9 alphanumeric characters');
        break;
    }

    // Add other necessary properties based on the selected style.
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Define CAPTCHA styles.
    $captcha_styles = [
      'captcha' => $this->t('6 to 9 alphanumeric characters'),
      'numerique6_7Captcha' => $this->t('6 to 7 numeric characters'),
      'alphabetique6_7Captcha' => $this->t('6 to 7 alphabetical characters'),
      'alphanumerique12Captcha' => $this->t('12 alphanumeric characters'),
      'alphabetique12Captcha' => $this->t('12 alphabetical characters'),
      'numerique12Captcha' => $this->t('12 numeric characters'),
      'alphanumerique4to6LightCaptcha' => $this->t('4 to 6 alphanumeric characters (light)'),
      'alphanumerique6to9LightCaptcha' => $this->t('6 to 9 alphanumeric characters (light)'),
    ];

    // Add CAPTCHA style select element to the form.
    $form['captcha_style'] = [
      '#type' => 'select',
      '#title' => $this->t('CAPTCHA Style'),
      '#default_value' => $this->getDefaultProperty('captcha_style'),
      '#options' => $captcha_styles,
    ];

    return $form;
  }

}
