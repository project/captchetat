<?php

namespace Drupal\captchetat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configure example settings for this site.
 */
class CaptchetatSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  const SETTINGS = 'captchetat.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'captchetat_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('API CaptchEtat Configuration'),
      '#weight' => 0,
      '#collapsible' => TRUE,
    ];
    $form['api_settings']['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];
    $form['api_settings']['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];
    $form['api_settings']['sandbox'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sandbox mode'),
      '#default_value' => $config->get('sandbox') ?? TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('oauth_url', $form_state->getValue('oauth_url'))
      ->set('sandbox', $form_state->getValue('sandbox'))
      ->save(TRUE);

    parent::submitForm($form, $form_state);
  }

}
