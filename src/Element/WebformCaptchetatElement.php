<?php

namespace Drupal\captchetat\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * Provides an element for the Captchetat.
 *
 * @RenderElement("captchetat")
 */
class WebformCaptchetatElement extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => FALSE,
      '#process' => [
        [$class, 'processWebformCaptchetat'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformCaptchetatElement'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformCaptchetatElement'],
      ],
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Processes a 'captchetat' element.
   */
  public static function processWebformCaptchetat(&$element, FormStateInterface $form_state, &$complete_form) {
    // Retrieve the selected CAPTCHA style from the element's properties.
    $config = \Drupal::config('captchetat.settings');
    $captcha_style = $element['#captcha_style'] ?? $config->get('captcha_style') ?? 'captcha';

    // Determine the current language and adjust the style name accordingly.
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $captcha_style = $captcha_style . ($lang === 'fr' ? 'FR' : 'EN');

    // Assign the selected CAPTCHA style to the element's attributes.
    $element['#attributes']['data-captchastylename'] = $captcha_style;

    return $element;
  }

  /**
   * Webform element validation handler for #type 'captchetat'.
   */
  public static function validateWebformCaptchetatElement(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
    $user_inputs = $form_state->getUserInput();
    $user_entered_captcha_code = strtoupper($user_inputs['captchetat']);
    $captcha_uuid = $user_inputs['captchetat-uuid'];

    if (empty($captcha_uuid) || empty($user_entered_captcha_code)) {
      return FALSE;
    }

    /** @var \Drupal\captchetat\Service\CaptchetatServiceInterface $captchetat */
    $captchetat = \Drupal::service('captchetat.captchetat');
    $token = $captchetat->getApiToken();
    $validation = $captchetat->validateCaptcha(
      $captcha_uuid, $user_entered_captcha_code, $token);

    // Check if the response is successful.
    if (!$validation) {
      $form_state->setErrorByName('captchetat', t('CAPTCHA validation failed.'));
    }
  }

  /**
   * Prepares a container render element for theme_element().
   */
  public static function preRenderWebformCaptchetatElement(array $element) {
    // Determine the current language and adjust the style name accordingly.
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();

    // Get the selected style name from element attributes.
    $style_name = $element['#attributes']['captchaStyleName'] ?? 'captcha' . ($lang === 'fr' ? 'FR' : 'EN');

    $element['#attached'] = [
      'library' => [
        'captchetat/captchetat',
      ],
    ];

    $element['captchetat'] = [
      '#type' => 'container',
      '#suffix' => '<p id="captchetat-hint" class="captchetat-hint">' . t('To display a new code or listen to the code, use the buttons next to the image.') . '</p>',
      '#attributes' => [
        'id' => 'captchetat',
        'captchaStyleName' => $style_name,
        'urlBackend' => URL::fromRoute(
          'captchetat.getcaptcha')->toString(),
      ],
    ];

    $element['captchetat_input'] = [
      '#type' => 'textfield',
      '#title' => t('Enter the security code'),
      '#description' => t('The code is composed of numbers and letters.'),
      '#description_display' => 'after',
      '#size' => 18,
      '#maxlength' => 12,
      '#required' => FALSE,
      '#attributes' => [
        'id' => 'captchaFormulaireExtInput',
        'name' => 'captchetat',
        'autocomplete' => 'off',
        'required' => 'required',
      ],
    ];
    return $element;
  }

}
