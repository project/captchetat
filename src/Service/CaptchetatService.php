<?php

namespace Drupal\captchetat\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Serialization\Yaml;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Service for calling CaptchEtat APIs.
 */
class CaptchetatService implements CaptchetatServiceInterface {

  use LoggerChannelTrait;

  const PISTE_API_URL = 'https://api.piste.gouv.fr';
  const PISTE_OAUTH_URL = 'https://oauth.piste.gouv.fr';
  const PISTE_SANDBOX_API_URL = 'https://sandbox-api.piste.gouv.fr';
  const PISTE_SANDBOX_OAUTH_URL = 'https://sandbox-oauth.piste.gouv.fr';
  const PISTE_API_PATH = '/piste/captchetat/v2';

  /**
   * The CaptchEtat module configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * HTTP client service.
   *
   * @var \GuzzleHttp\Client
   */
  protected Client $httpClient;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Constructs a new CaptchetatService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\Client $http_client
   *   HTTP client service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Client $http_client,
    CacheBackendInterface $cache,
  ) {
    $this->config = $config_factory->get('captchetat.settings');
    $this->httpClient = $http_client;
    $this->cache = $cache;
  }

  /**
   * Returns the CaptchEtat API url.
   *
   * @return string
   *   The CaptchEtat API url.
   */
  protected function getApiUrl(): string {
    // Defaults to sandbox mode if config missing.
    $sandbox = $this->config->get('sandbox') ?? TRUE;
    return $sandbox ? self::PISTE_SANDBOX_API_URL : self::PISTE_API_URL;
  }

  /**
   * Returns the CaptchEtat OAuth url.
   *
   * @return string
   *   The CaptchEtat OAuth url.
   */
  protected function getOauthUrl(): string {
    // Defaults to sandbox mode if config missing.
    $sandbox = $this->config->get('sandbox') ?? TRUE;
    return $sandbox ? self::PISTE_SANDBOX_OAUTH_URL : self::PISTE_OAUTH_URL;
  }

  /**
   * {@inheritDoc}
   */
  public function clearApiToken(): void {
    $this->cache->delete('captchetat.access_token');
  }

  /**
   * {@inheritDoc}
   */
  public function fetchApiToken(): string {
    $client_id = $this->config->get('client_id');
    $client_secret = $this->config->get('client_secret');
    $oauth_url = $this->getOauthUrl();
    if (empty($client_id) || empty($client_secret) || empty($oauth_url)) {
      $this->getLogger('captchetat')->warning('Missing configuration');
      return '';
    }
    $url = $oauth_url . '/api/oauth/token';
    $data = [
      'grant_type' => 'client_credentials',
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'scope' => 'piste.captchetat',
    ];
    try {
      $response = $this->httpClient->post($url, ['form_params' => $data]);
      $contents = $response->getBody()->getContents();
      $token_data = json_decode($contents);
      $token = $token_data->access_token;
      $expires_in = $token_data->expires_in;
      // Estimate access token expiration time.
      $expiration = time() + ($expires_in / 2);
      // Store access token in Drupal cache.
      // Adding config cache tags will allow auto-refresh on config update.
      $this->cache->set(
        'captchetat.access_token',
        $token,
        $expiration,
        $this->config->getCacheTags(),
      );
      // Return access token.
      return $token;
    }
    catch (\Throwable $e) {
      $this->getLogger('captchetat')->error($e->getMessage());
      return '';
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getApiToken(): string {
    // Check if CaptchEtat access token is present in Drupal cache.
    $access_token_cache = $this->cache->get('captchetat.access_token');
    if ($access_token_cache) {
      // Return access token found in cache.
      return $access_token_cache->data;
    }
    else {
      // Fetch a new CaptchEtat access token.
      return $this->fetchApiToken();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function getCaptcha(string $token, Request $request): Response {
    if (empty($token)) {
      $this->getLogger('captchetat')->warning('Empty access token.');
      return new Response('Error: invalid access token', 500);
    }
    $request_uri = $request->getRequestUri();
    $query_params = explode('?', $request_uri)[1];
    $url = $this->getApiUrl() . self::PISTE_API_PATH . '/simple-captcha-endpoint?' . $query_params;
    try {
      $headers = ['Authorization' => "Bearer $token"];
      $response = $this->httpClient->get($url, ['headers' => $headers]);
      $content_type = $response->getHeaders()['content-type'][0];
      return new Response(
        $response->getBody()->getContents(),
        $response->getStatusCode(),
        ['Content-Type' => $content_type]
      );
    }
    catch (\Throwable $e) {
      $this->getLogger('captchetat')->error($e->getMessage());
      return new Response('Error: ' . $e->getMessage(), 500);
    }
  }

  /**
   * {@inheritDoc}
   */
  public function healthCheck(string $token): mixed {
    if (empty($token)) {
      $this->getLogger('captchetat')->warning('Empty access token.');
      return FALSE;
    }
    $url = $this->getApiUrl() . self::PISTE_API_PATH . '/healthcheck';
    $headers = ['Authorization' => "Bearer $token"];
    try {
      $response = $this->httpClient->get($url, ['headers' => $headers]);
      $contents = $response->getBody()->getContents();
      // Returns status info if available.
      if (empty($contents)) {
        return FALSE;
      }
      else {
        return Yaml::decode($contents);
      }
    }
    catch (\Throwable $e) {
      $this->getLogger('captchetat')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function isServiceUp(string $token): bool {
    $status_info = $this->healthCheck($token);
    if ($status_info && isset($status_info['status'])) {
      // Check if the status is UP.
      return $status_info['status'] === 'UP';
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function validateCaptcha(
    string $captcha_uuid,
    string $user_entered_captcha_code,
    string $token,
  ): bool {
    if (empty($user_entered_captcha_code)) {
      // Captcha is invalid if user entered code is empty, no need to verify.
      return FALSE;
    }
    elseif (empty($token)) {
      $this->getLogger('captchetat')->warning('Empty access token.');
      return FALSE;
    }
    elseif (empty($captcha_uuid)) {
      $this->getLogger('captchetat')->warning('Empty captcha uuid.');
      return FALSE;
    }

    $url = $this->getApiUrl() . self::PISTE_API_PATH . '/valider-captcha';

    $data = [
      'uuid' => $captcha_uuid,
      'code' => $user_entered_captcha_code,
    ];

    $jsonData = json_encode($data);

    $headers = [
      'Authorization' => "Bearer $token",
      'Content-Type' => 'application/json',
    ];

    try {
      $response = $this->httpClient->post(
        $url, ['headers' => $headers, 'body' => $jsonData]);
      // Check if validation is successful.
      return $response->getBody()->getContents() === 'true';
    }
    catch (\Throwable $e) {
      $this->getLogger('captchetat')->error($e->getMessage());
      return FALSE;
    }
  }

}
